﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using task2.Models;

namespace task2.I
{
	public interface IMediator
	{
		void LandRequest(Aircraft aircraft, Runway runway);
		void TakeOffRequest(Aircraft aircraft, Runway runway);
	}

}
