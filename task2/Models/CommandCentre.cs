﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using task2.I;

namespace task2.Models
{
	public class CommandCentre : IMediator
	{
		private readonly List<Runway> _runways;
		private readonly List<Aircraft> _aircrafts;

		public CommandCentre(Runway[] runways, Aircraft[] aircrafts)
		{
            Console.OutputEncoding = Encoding.UTF8;

            _runways = new List<Runway>(runways);
			_aircrafts = new List<Aircraft>(aircrafts);
			foreach (var aircraft in _aircrafts)
			{
				aircraft.SetMediator(this);
			}
		}

		public void LandRequest(Aircraft aircraft, Runway runway)
		{
			if (runway.IsAvailable())
			{
				runway.LandAircraft(aircraft);
			}
			else
			{
				Console.WriteLine($"Aircraft {aircraft.Name}: Не вдалося приземлитися, злітно-посадкова смуга {runway.Id} зайнята.");
			}
		}

		public void TakeOffRequest(Aircraft aircraft, Runway runway)
		{
			if (runway.IsBusyWith(aircraft))
			{
				runway.TakeOffAircraft(aircraft);
			}
			else
			{
				Console.WriteLine($"Aircraft {aircraft.Name}: Не можу злітати, зараз не знаходжусь на злітно-посадковій смузі {runway.Id}.");
			}
		}
	}
}
