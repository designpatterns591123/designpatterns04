﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using task2.I;

namespace task2.Models
{
	public class Aircraft(string name)
	{
		public string Name { get; set; } = name;
		public IMediator? Mediator { get; private set; }

		public void Land(Runway runway)
		{
			Mediator?.LandRequest(this, runway);
		}

		public void TakeOff(Runway runway)
		{
			Mediator?.TakeOffRequest(this, runway);
		}

		public void SetMediator(IMediator mediator)
		{
			Mediator = mediator;
		}
	}
}
