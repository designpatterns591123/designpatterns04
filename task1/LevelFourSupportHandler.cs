﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace task1
{
	class LevelFourSupportHandler : SupportHandler
	{
		public override void HandleRequest(SupportRequest request)
		{
            Console.OutputEncoding = Encoding.UTF8;

            if (request.Level == 5)
			{
				Console.WriteLine("Рівень 4: Питання вирішено");
			}
			else if (NextHandler != null)
			{
				NextHandler.HandleRequest(request);
			}
			else
			{
				Console.WriteLine("Питання не можливо вирішити");
			}
		}
	}
}
