﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace task1
{
	class SupportRequest
	{
		public int Level { get; set; }
		public SupportRequest(int level)
		{
			Level = level;
		}
	}
}
