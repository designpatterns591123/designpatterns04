﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace task1
{
	abstract class SupportHandler
	{
		protected SupportHandler NextHandler;

		public void SetNextHandler(SupportHandler nextHandler)
		{
			NextHandler = nextHandler;
		}

		public abstract void HandleRequest(SupportRequest request);
	}
}
