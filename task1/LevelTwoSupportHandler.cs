﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace task1
{
	class LevelTwoSupportHandler : SupportHandler
	{
		public override void HandleRequest(SupportRequest request)
        {
            Console.OutputEncoding = Encoding.UTF8;

            if (request.Level == 2)
			{
				Console.WriteLine("Рівень 2: Питання вирішено");
			}
			else if (NextHandler != null)
			{
				NextHandler.HandleRequest(request);
			}
			else
			{
				Console.WriteLine("Питання не можливо вирішити");
			}
		}
	}
}
