﻿using task1;

SupportHandler levelOneHandler = new LevelOneSupportHandler();
SupportHandler levelTwoHandler = new LevelTwoSupportHandler();
SupportHandler levelThreeHandler = new LevelThreeSupportHandler();
SupportHandler levelFourHandler = new LevelFourSupportHandler();

levelOneHandler.SetNextHandler(levelTwoHandler);
levelTwoHandler.SetNextHandler(levelThreeHandler);
levelThreeHandler.SetNextHandler(levelFourHandler);

SupportRequest request1 = new (2);
SupportRequest request2 = new (3);
SupportRequest request3 = new (1);
SupportRequest request4 = new (5);

levelOneHandler.HandleRequest(request1);
levelOneHandler.HandleRequest(request2);
levelOneHandler.HandleRequest(request3);
levelOneHandler.HandleRequest(request4);

Console.ReadLine();