﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using task3.I;

namespace task3.Models.Image
{
    public class FileSystemImageLoader : IImageLoader
	{
		public void LoadImage(LightHtmlImageNode image)
		{
			image.Content = File.ReadAllBytes(image.Src);
		}
	}
}
