﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace task3.Models.Element
{
    public class LightHtmlElementNode(string tagName) : LightHtmlElement
    {
        private readonly string tagName = tagName;
        private readonly List<LightHtmlElement> children = [];

        public void AddChild(LightHtmlElement child)
        {
            children.Add(child);
        }

        public override void Render()
        {
            Console.Write($"<{tagName}>");
            foreach (var child in children)
            {
                child.Render();
            }
            Console.WriteLine($"</{tagName}>");
        }
    }
}
