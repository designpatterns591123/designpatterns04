﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace task3.Models
{
	public class EventListener(string eventType, Action eventHandler)
	{
		public string EventType { get; set; } = eventType;
		public Action EventHandler { get; set; } = eventHandler;
	}
}
