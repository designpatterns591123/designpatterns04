﻿using System.Xml.Linq;
using task3.Models;
using task3.Models.Element;
using task3.Models.Image;
using task3.Models.Text;
using static System.Net.Mime.MediaTypeNames;

// Завданння 3
LightElementNode body = new("body", "block", "closing", []); 
LightElementNode div = new("div", "block", "closing", ["container"]);
LightElementNode h1 = new("h1", "block", "closing", []);
LightTextNode text = new("Hello, LightHTML!");

div.AddEventListener("click", () =>
{
	Console.WriteLine("click!");
});

div.AddEventListener("mouseover", () =>
{
	Console.WriteLine("mouseover!");
});

div.Click();
div.MouseOver();


// Завданння 4
var networkImage = new LightHtmlImageNode("https://http.cat/images/102.jpg"); 

networkImage.Render();