﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace task5.Models
{
    // Клас Memento, що зберігає стан текстового документа
    public class Memento
    {
        private readonly string textSnapshot;

        public Memento(string textSnapshot)
        {
            this.textSnapshot = textSnapshot;
        }

        public string TextSnapshot => textSnapshot;
    }
}