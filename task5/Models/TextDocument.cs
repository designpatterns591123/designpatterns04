﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace task5.Models
{
    // Клас TextDocument, що представляє текстовий документ
    public class TextDocument
    {
        private string text;

        public TextDocument(string initialText)
        {
            text = initialText;
        }

        public string Text
        {
            get => text;
            set => text = value;
        }
    }
}
