﻿using System;
using System.Text;

namespace task5
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.OutputEncoding = Encoding.UTF8;

            var document = new Models.TextDocument("Привіт, світ!");
            var editor = new Models.TextEditor(document);

            Console.WriteLine(editor.Text); // Привіт, світ!

            editor.Write(" Звідки ти?"); 
            Console.WriteLine(editor.Text); // Привіт, світ! Звідки ти?

            editor.Undo();
            Console.WriteLine(editor.Text); // Привіт, світ!
        }
    }
}
